import netsquid.qubits.qubitapi as qapi
from netsquid.components.instructions import Instruction
from netsquid.qubits import operators as ops


class IInitBell(Instruction):
    """Instruction that initializes two qubits in a Bell state.

    """

    @property
    def name(self):
        """str: instruction name."""
        return "init_bell_op"

    @property
    def num_positions(self):
        """int: number of targeted memory positions_of_connections. If -1, number is unrestricted."""
        return 2

    def execute(self, quantum_memory, positions, Bell_state=1, **kwargs):
        """....

        Expects two positions_of_connections...

        Parameters
        ----------
        quantum_memory : :obj:`~netsquid.components.qmemory.QuantumMemory`
            Quantum memory to execute instruction on.
        positions : list of int
            Memory positions_of_connections to do instruction on.
        Bell_state: index of Bell state to initialize
            0: phi+ (default) 1:psi+  2:phi-  3:psi-

        """
        if len(positions) != 2:
            raise ValueError("Bell state must be initialized over two qubits")
        if Bell_state not in [0, 1, 2, 3]:
            raise ValueError("Bell state index is invalid (choose 0, 1, 2 or 3)")

        # Initialize phi+
        q1, q2 = qapi.create_qubits(2)
        qapi.operate([q1], ops.H)
        qapi.operate([q1, q2], ops.CNOT)

        # Apply X and/or Z to turn it into desired Bell state
        if Bell_state in [2, 3]:
            qapi.operate([q1], ops.Z)
        if Bell_state in [1, 3]:
            qapi.operate([q2], ops.X)

        quantum_memory.put([q1, q2], positions=positions, replace=True,
                           check_positions=True)


# Defining usable (physical) instructions
INSTR_INIT_BELL = IInitBell()
