from __future__ import annotations

from typing import Optional

from netsquid.components.instructions import INSTR_MEASURE, INSTR_INIT, INSTR_ROT, \
    INSTR_H, INSTR_I, INSTR_CNOT, INSTR_SWAP, INSTR_ROT_Z, INSTR_EMIT
from netsquid.components.models.qerrormodels import T1T2NoiseModel, DepolarNoiseModel
from netsquid.components.qprocessor import QuantumProcessor, PhysicalInstruction
from netsquid_driver.measurement_services import MeasureService, SwapService
from netsquid_driver.memory_manager_implementations import MemoryManagerWithMoveProgram
from netsquid_driver.memory_manager_service import QuantumMemoryManager
from netsquid_driver.operation_services import ProcessingNodeSwapService, ProcessingNodeMeasureService
from netsquid_netbuilder.modules.qdevices.interface import IQDeviceBuilder, IQDeviceConfig

from netsquid_abstractmodel.programs import AbstractMoveProgram, AbstractBellStateMeasurementProgram


class AbstractQDeviceConfig(IQDeviceConfig):
    """
    Parameter configuration for an abstract QuantumProcessor.

    Default values were obtained by mapping from NV center parameters achievable in 2020 to this abstract model.
    For details see appendices of "Optimizing Entanglement Generation and Distribution using Genetic Algorithms".
    """

    num_positions: int
    """Number of qubits in the processor."""
    swap_quality: float = 0.85
    """Parametrizes the depolarizing noise introduced by the node's processor during an entanglement swap"""
    T1: float = 3.6e18
    """Relaxation time of the node's qubits"""
    T2: float = 1e15
    """Dephasing time of the node's qubits"""
    initialization_duration: float = 2000.
    """Time in ns needed to initialise a qubit in the memory."""
    single_qubit_gate_duration: float = 5.
    """Time in ns needed to perform a single qubit gate in the memory."""
    two_qubit_gate_duration: float = 1.542e6
    """Time in ns needed to perform a two qubit gate in the memory."""
    measurement_duration: float = 3700.
    """Time in ns needed to measure a qubit in the memory."""
    emission_duration: float = 3500.
    """Time in ns needed to emit a photon."""

    external_params: Optional[dict] = None
    """
    Dictionary of extra parameters that have no direct impact on the qdevice model,
    but are physically associated with the qdevice.
    It is typically used for parameters that relate to the entanglement generation, such as `emission_fidelity`.
    """

    def make_perfect(self):
        self.T1 = 0.
        self.T2 = 0.
        self.swap_quality = 1

    def make_instant(self):
        self.initialization_duration = 0
        self.single_qubit_gate_duration = 0
        self.two_qubit_gate_duration = 0
        self.measurement_duration = 0
        self.emission_duration = 0


class AbstractProcessor(QuantumProcessor):
    """Abstract QuantumProcessor.

    This is a simple model for a QuantumProcessor. Qubits are fully connected and all suffer from T1,T2
    memory decoherence. Operation noise is depolarizing and quantified by a swap_quality parameter. This noise
    is applied only through an identity gate, whose duration is set to 0. All other operations are noiseless, so
    one should include identity gates whenever noise modelling is desired.

    This processor supports arbitrary single-qubit rotations, Hadamard, CNOT and SWAP gates on all of its qubits.
    All qubits can also be initialized and measured with no restrictions. Emission is allowed from all positions as
    well.
    """

    def __init__(self, qdevice_cfg: AbstractQDeviceConfig):
        super().__init__(name="abstract_processor",
                         num_positions=qdevice_cfg.num_positions,
                         mem_noise_models=T1T2NoiseModel(T1=qdevice_cfg.T1, T2=qdevice_cfg.T2))

        if qdevice_cfg.external_params:
            for property_name, value in qdevice_cfg.external_params.items():
                self.add_property(property_name, value)

        self.emission_position = qdevice_cfg.num_positions

        physical_measurement = PhysicalInstruction(instruction=INSTR_MEASURE,
                                                   duration=qdevice_cfg.measurement_duration,
                                                   q_noise_model=None,
                                                   c_noise_model=None,
                                                   parallel=False)

        physical_init = PhysicalInstruction(instruction=INSTR_INIT,
                                            duration=qdevice_cfg.initialization_duration,
                                            q_noise_model=None,
                                            c_noise_model=None,
                                            parallel=True,
                                            apply_q_noise_after=True)

        rotation_gate = PhysicalInstruction(instruction=INSTR_ROT,
                                            duration=qdevice_cfg.single_qubit_gate_duration,
                                            q_noise_model=None,
                                            c_noise_model=None,
                                            parallel=True,
                                            apply_q_noise_after=True)

        z_rotation_gate = PhysicalInstruction(instruction=INSTR_ROT_Z,
                                              duration=qdevice_cfg.single_qubit_gate_duration,
                                              q_noise_model=None,
                                              c_noise_model=None,
                                              parallel=True,
                                              apply_q_noise_after=True)

        h_gate = PhysicalInstruction(instruction=INSTR_H,
                                     duration=qdevice_cfg.single_qubit_gate_duration,
                                     q_noise_model=None,
                                     c_noise_model=None,
                                     parallel=True,
                                     apply_q_noise_after=True)

        i_gate = PhysicalInstruction(instruction=INSTR_I,
                                     duration=0.,
                                     q_noise_model=DepolarNoiseModel(1. - qdevice_cfg.swap_quality,
                                                                     time_independent=True),
                                     c_noise_model=None,
                                     parallel=True,
                                     apply_q_noise_after=True)

        cnot_gate = PhysicalInstruction(instruction=INSTR_CNOT,
                                        duration=qdevice_cfg.two_qubit_gate_duration,
                                        q_noise_model=None,
                                        c_noise_model=None,
                                        parallel=True,
                                        apply_q_noise_after=True)

        magical_swap_gate = PhysicalInstruction(instruction=INSTR_SWAP,
                                                duration=qdevice_cfg.two_qubit_gate_duration,
                                                q_noise_model=None,
                                                c_noise_model=None,
                                                parallel=True,
                                                apply_q_noise_after=True)

        emit_instruction = PhysicalInstruction(instruction=INSTR_EMIT,
                                               duration=qdevice_cfg.emission_duration)

        # emit instruction is not used in magic models, but they can fetch the property emission_duration from
        # the qdevice if supported. (Thus we add it here as a property)
        self.add_property("emission_duration", qdevice_cfg.emission_duration)

        self.add_physical_instruction(physical_measurement)
        self.add_physical_instruction(physical_init)
        self.add_physical_instruction(rotation_gate)
        self.add_physical_instruction(z_rotation_gate)
        self.add_physical_instruction(h_gate)
        self.add_physical_instruction(i_gate)
        self.add_physical_instruction(cnot_gate)
        self.add_physical_instruction(magical_swap_gate)
        self.add_physical_instruction(emit_instruction)


class AbstractQdeviceBuilder(IQDeviceBuilder):
    """Abstract QuantumProcessor Builder."""

    @classmethod
    def build_services(cls, node):
        if node.qmemory is None:
            return
        driver = node.driver
        driver.add_service(MeasureService, ProcessingNodeMeasureService(node=node))
        driver.add_service(SwapService, ProcessingNodeSwapService(node=node,
                                                                  swap_prog=AbstractBellStateMeasurementProgram))
        driver.add_service(
            QuantumMemoryManager,
            MemoryManagerWithMoveProgram(node=node, move_program=AbstractMoveProgram()),
        )

    @classmethod
    def build(cls, name: str, qdevice_cfg: AbstractQDeviceConfig) -> QuantumProcessor:
        if isinstance(qdevice_cfg, dict):
            qdevice_cfg = AbstractQDeviceConfig(**qdevice_cfg)

        processor = AbstractProcessor(qdevice_cfg)

        return processor
