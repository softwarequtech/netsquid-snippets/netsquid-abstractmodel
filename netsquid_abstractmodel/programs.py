from netsquid import BellIndex
from netsquid.components.instructions import INSTR_SWAP, INSTR_CNOT, INSTR_I, INSTR_H, INSTR_MEASURE
from netsquid.components.qprogram import QuantumProgram
from netsquid_driver.operation_services import IBellStateMeasurementProgram


class AbstractMoveProgram(QuantumProgram):
    def program(self):
        move_from, move_to = self.get_qubit_indices(2)
        self.apply(INSTR_SWAP, [move_from, move_to])
        yield self.run()


class AbstractBellStateMeasurementProgram(IBellStateMeasurementProgram):
    """
    QuantumProgram that represents a quantum circuit which measures
    in the Bell basis by applying a CNOT and a Hadamard, followed
    by measuring both qubits in the standard basis. Depolarizing noise is applied
    by means of an identity operation on both qubits before measurement.
    """

    _OUTCOME_TO_BELL_INDEX = {(0, 0): 0, (0, 1): 1, (1, 0): 2, (1, 1): 3}
    _translate = [BellIndex.PHI_PLUS, BellIndex.PSI_PLUS, BellIndex.PHI_MINUS, BellIndex.PSI_MINUS]

    def program(self):
        control, target = self.get_qubit_indices(2)
        self.apply(INSTR_CNOT, [control, target])
        self.apply(INSTR_H, control)
        self.apply(INSTR_I, control)
        self.apply(INSTR_I, target)
        for (qubit, name) in [(control, self._NAME_OUTCOME_CONTROL),
                              (target, self._NAME_OUTCOME_TARGET)]:
            self.apply(INSTR_MEASURE, qubit, name, inplace=False)
        yield self.run()

    @property
    def outcome_as_netsquid_bell_index(self):
        old_bell_index = self.get_outcome_as_bell_index
        return self._translate[old_bell_index]


class AbstractOneQubitHadamard(QuantumProgram):

    default_num_qubits = 1

    def program(self):
        q = self.get_qubit_indices()
        self.apply(instruction=INSTR_H, qubit_indices=q)
        yield self.run()
