CHANGELOG
=========

2025-02-19 (2.0.0)
--------------------

- Support integration with netsquid-netbuilder 0.1.0
- Add AbstractQDeviceConfig and AbstractQdeviceBuilder
- Change initialization of AbstractProcessor to use AbstractQDeviceConfig object
- Removed `AbstractParameterSet` class


2021-06-08 (1.3.0)
------------------

- Added emit instruction

2021-03-24 (1.2.0)
------------------

- Added `AbstractParameterSet` class

2021-01-27 (1.1.2)
-----------------

- Measurement operator can no longer be done in parallel

2021-01-08 (1.1.1)
------------------

- Operation durations are now correctly passed upon initialization
- swap quality parameter is now interpreted as a probability instead of a rate of depolarization

2020-10-21 (1.1.0)
------------------

- Added support for rotations around arbitrary axes

2020-10-20 (1.0.0)
------------------

- `AbstractNode` now inherits from the `NetSquid` `Node` class instead of from the old `netsquid-physlayer` `QuantumNode` class
- Removed legacy `netsquid-netconf` code
- Updated requirements: snippet no longer depends on `netsquid-physlayer` and `netsquid-netconf`
 
2020-05-08 (0.0.0)
------------------

Example changelog entry

- Created this snippet
