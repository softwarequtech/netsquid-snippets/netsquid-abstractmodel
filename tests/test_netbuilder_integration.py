import unittest

import netsquid as ns
from netsquid_netbuilder.modules.clinks import DefaultCLinkConfig
from netsquid_netbuilder.modules.qlinks import PerfectQLinkConfig, HeraldedDoubleClickQLinkConfig
from netsquid_netbuilder.run import get_default_builder, run
from netsquid_netbuilder.util.fidelity import calculate_fidelity_epr
from netsquid_netbuilder.util.network_generation import create_2_node_network, create_metro_hub_network, \
    create_two_connected_metro_hubs_network
from netsquid_netbuilder.util.test_protocol_qlink import CreateAndKeepSenderProtocol, CreateAndKeepEventRegistration, \
    CreateAndKeepReceiveProtocol, \
    MeasureDirectlyReceiveProtocol, MeasureDirectlySenderProtocol, MeasureDirectlyEventRegistration
from qlink_interface import ResCreateAndKeep

from netsquid_abstractmodel.abstract_node import AbstractQDeviceConfig, \
    AbstractQdeviceBuilder


class TestNetbuilderIntegration(unittest.TestCase):
    def setUp(self) -> None:
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)

        self.builder = get_default_builder()
        self.builder.register(model_name="abstract", builder=AbstractQdeviceBuilder, config=AbstractQDeviceConfig)

    def tearDown(self) -> None:
        pass

    def check_epr_pairs(
            self,
            results_reg: CreateAndKeepEventRegistration,
            expected_n_epr: int,
            expected_fidelity: float,
            fidelity_delta: float
    ):

        self.assertEqual(len(results_reg.received_ck), expected_n_epr * 2)

        for i in range(expected_n_epr):
            res_egp_1 = results_reg.received_ck[i * 2]

            res_create_keep: ResCreateAndKeep = res_egp_1.result

            fid = calculate_fidelity_epr(res_egp_1.dm, res_create_keep.bell_state)

            self.assertAlmostEqual(fid, expected_fidelity, delta=fidelity_delta)

    def check_measure_directly(self, event_register: MeasureDirectlyEventRegistration):
        self.assertEqual(len(event_register.submitted_md) * 2, len(event_register.received_md))

        for event in event_register.received_md:
            mirror_events = [
                temp_evt
                for temp_evt in event_register.received_md
                if temp_evt.peer_name == event.node_name
                and temp_evt.node_name == event.peer_name
                and temp_evt.result.create_id == event.result.create_id
            ]
            self.assertEqual(len(mirror_events), 1)
            mirror_event = mirror_events[0]

            if event.result.bell_state in [ns.BellIndex.PHI_PLUS, ns.BellIndex.PHI_MINUS]:
                self.assertEqual(
                    event.result.measurement_outcome, mirror_event.result.measurement_outcome
                )
            else:
                assert event.result.bell_state in [ns.BellIndex.PSI_PLUS, ns.BellIndex.PSI_MINUS]
                self.assertNotEqual(
                    event.result.measurement_outcome, mirror_event.result.measurement_outcome
                )

    def test_qlink_perfect(self):
        delay = 5325.73

        qdevice_cfg = AbstractQDeviceConfig(num_positions=2)
        qlink_cfg = PerfectQLinkConfig(state_delay=delay)
        network_cfg = create_2_node_network(qlink_typ="perfect", qlink_cfg=qlink_cfg,
                                            qdevice_typ="abstract", qdevice_cfg=qdevice_cfg)
        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        alice_program = CreateAndKeepSenderProtocol("Bob", results_reg, n_epr)
        bob_program = CreateAndKeepReceiveProtocol("Alice", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Alice": alice_program, "Bob": bob_program})

        self.check_epr_pairs(
            results_reg,
            n_epr,
            expected_fidelity=1,
            fidelity_delta=1e-9,
        )

    def test_mh(self):
        node_names = ["Node0", "Node1", "Node2", "Node3"]
        node_distances = [10, 13.4, 33, 0.8]

        qdevice_cfg = AbstractQDeviceConfig(num_positions=2)
        qlink_cfg = HeraldedDoubleClickQLinkConfig(
            p_loss_init=0.5,
            p_loss_length=0,
        )
        network_cfg = create_metro_hub_network(
            node_names=node_names, node_distances=node_distances,
            qlink_typ="heralded-double-click", qlink_cfg=qlink_cfg,
            qdevice_typ="abstract", qdevice_cfg=qdevice_cfg)

        results_reg = CreateAndKeepEventRegistration()
        n_epr = 50

        node0_program = CreateAndKeepSenderProtocol("Node1", results_reg, n_epr)
        node1_program = CreateAndKeepReceiveProtocol("Node0", results_reg, n_epr)
        node2_program = CreateAndKeepSenderProtocol("Node3", results_reg, n_epr)
        node3_program = CreateAndKeepReceiveProtocol("Node2", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Node0": node0_program, "Node1": node1_program, "Node2": node2_program, "Node3": node3_program})

        self.check_epr_pairs(
            results_reg,
            expected_n_epr=n_epr * 2,
            expected_fidelity=1,
            fidelity_delta=1e-9,
        )

    def test_repeater_chain(self):
        hub1_names = ["Node0", "Node1"]
        hub2_names = ["Node2", "Node3"]
        hub1_distances = [10, 13.4]
        hub2_distances = [33, 0.8]
        repeater_chain_distances = [24, 22, 10.5]

        qdevice_cfg = AbstractQDeviceConfig(num_positions=2)
        qdevice_cfg.make_perfect()
        qdevice_cfg.make_instant()

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_names, hub1_node_distances=hub1_distances,
            hub2_node_names=hub2_names, hub2_node_distances=hub2_distances,
            repeater_chain_distances=repeater_chain_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(),
            clink_typ="default",
            clink_cfg=DefaultCLinkConfig(),
            qdevice_typ="abstract", qdevice_cfg=qdevice_cfg)

        results_reg = CreateAndKeepEventRegistration()
        n_epr = 10

        node0_program = CreateAndKeepSenderProtocol("Node3", results_reg, n_epr)
        node1_program = CreateAndKeepReceiveProtocol("Node2", results_reg, n_epr)
        node2_program = CreateAndKeepSenderProtocol("Node1", results_reg, n_epr)
        node3_program = CreateAndKeepReceiveProtocol("Node0", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Node0": node0_program, "Node1": node1_program, "Node2": node2_program, "Node3": node3_program})

        self.check_epr_pairs(
            results_reg,
            expected_n_epr=n_epr * 2,
            expected_fidelity=1,
            fidelity_delta=1e-9,
        )

    def test_repeater_chain_ck(self):
        hub1_names = ["Node0", "Node1"]
        hub2_names = ["Node2", "Node3"]
        hub1_distances = [10, 13]
        hub2_distances = [22, 1]
        repeater_chain_distances = [24, 22, 10.5]

        qdevice_cfg = AbstractQDeviceConfig(num_positions=2)
        qdevice_cfg.make_perfect()

        network_cfg = create_two_connected_metro_hubs_network(
            hub1_node_names=hub1_names, hub1_node_distances=hub1_distances,
            hub2_node_names=hub2_names, hub2_node_distances=hub2_distances,
            repeater_chain_distances=repeater_chain_distances,
            qlink_typ="perfect",
            qlink_cfg=PerfectQLinkConfig(),
            qdevice_typ="abstract", qdevice_cfg=qdevice_cfg)

        results_reg = MeasureDirectlyEventRegistration()
        n_epr = 10

        node0_program = MeasureDirectlySenderProtocol("Node3", results_reg, n_epr)
        node1_program = MeasureDirectlyReceiveProtocol("Node2", results_reg, n_epr)
        node2_program = MeasureDirectlySenderProtocol("Node1", results_reg, n_epr)
        node3_program = MeasureDirectlyReceiveProtocol("Node0", results_reg, n_epr)

        network = self.builder.build(network_cfg)
        run(network, {"Node0": node0_program, "Node1": node1_program, "Node2": node2_program, "Node3": node3_program})

        self.check_measure_directly(results_reg)


if __name__ == "__main__":
    unittest.main()
