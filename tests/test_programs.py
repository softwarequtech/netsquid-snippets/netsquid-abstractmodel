import math
import unittest

import netsquid as ns
from netsquid.components import INSTR_INIT, INSTR_ROT
from netsquid_netbuilder.run import get_default_builder
from netsquid_netbuilder.util.network_generation import create_single_node_network

from netsquid_abstractmodel.abstract_node import AbstractQDeviceConfig, \
    AbstractQdeviceBuilder
from netsquid_abstractmodel.instructions import INSTR_INIT_BELL
from netsquid_abstractmodel.programs import AbstractBellStateMeasurementProgram, AbstractMoveProgram


class TestAbstractPrograms(unittest.TestCase):

    def setUp(self) -> None:
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)
        qdevice_config = AbstractQDeviceConfig(num_positions=2)
        qdevice_config.make_perfect()
        builder = get_default_builder()
        builder.register(model_name="abstract", builder=AbstractQdeviceBuilder, config=AbstractQDeviceConfig)
        network_config = create_single_node_network("abstract", qdevice_config)

        self.abstract_node = builder.build(network_config).end_nodes["Alice"]

        self.abstract_node.qmemory.add_instruction(INSTR_INIT_BELL, duration=0)

    def tearDown(self) -> None:
        ns.sim_stop()

    def test_swap(self):
        swap_program = AbstractBellStateMeasurementProgram()

        for BellStateIndex in [0, 1, 2, 3]:  # 0: phi+, 1: psi+, 2: phi-, 3:psi-

            self.abstract_node.qmemory.execute_instruction(INSTR_INIT_BELL, Bell_state=BellStateIndex)
            ns.sim_run()
            self.abstract_node.qmemory.execute_program(swap_program, qubit_mapping=[0, 1])
            ns.sim_run()
            self.assertEqual(swap_program.get_outcome_as_bell_index, BellStateIndex)

    def test_move(self):
        # Create two qubits, first in up and second in down
        self.abstract_node.qmemory.execute_instruction(INSTR_INIT, [0], physical=False)
        self.abstract_node.qmemory.execute_instruction(INSTR_ROT, [0], angle=math.pi, axis=(1, 0, 0), physical=False)
        self.abstract_node.qmemory.execute_instruction(INSTR_INIT, [1], physical=False)

        move_program = AbstractMoveProgram()
        self.abstract_node.qmemory.execute_program(move_program, qubit_mapping=[0, 1])
        ns.sim_run()
        output = self.abstract_node.qmemory.measure(positions=[0, 1])

        # Check that qubits have been switched, so: first qubit is down, second is up
        self.assertEqual(output[0][0], 0)
        self.assertEqual(output[0][1], 1)


if __name__ == "__main__":
    unittest.main()
