import unittest

import netsquid as ns
from netsquid.components.instructions import INSTR_MEASURE, INSTR_INIT, INSTR_H, INSTR_CNOT, INSTR_EMIT
from netsquid_netbuilder.run import get_default_builder
from netsquid_netbuilder.util.network_generation import create_single_node_network

from netsquid_abstractmodel.abstract_node import AbstractQDeviceConfig, \
    AbstractQdeviceBuilder
from netsquid_abstractmodel.programs import AbstractBellStateMeasurementProgram


class TestAbstractNode(unittest.TestCase):

    @staticmethod
    def create_node(qdevice_config: AbstractQDeviceConfig) -> ns.nodes.Node:
        builder = get_default_builder()
        builder.register(model_name="abstract", builder=AbstractQdeviceBuilder, config=AbstractQDeviceConfig)
        network_config = create_single_node_network("abstract", qdevice_config)

        return builder.build(network_config).end_nodes["Alice"]

    def setUp(self) -> None:
        ns.sim_reset()
        ns.set_qstate_formalism(ns.QFormalism.DM)

    def tearDown(self) -> None:
        ns.sim_stop()

    def test_swap_quality(self):
        swap_quality = 0.7
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=swap_quality, T1=0, T2=0)
        qdevice_config.make_instant()
        node = self.create_node(qdevice_config)

        swap_program = AbstractBellStateMeasurementProgram()

        qubits_a = ns.qubits.create_qubits(2)
        ns.qubits.qubitapi.assign_qstate(qubits_a, ns.b00)

        qubits_b = ns.qubits.create_qubits(2)
        ns.qubits.qubitapi.assign_qstate(qubits_b, ns.b00)

        node.qmemory.put(qubits_a[0], positions=[0])
        node.qmemory.put(qubits_b[0], positions=[1])

        node.qmemory.execute_program(swap_program, qubit_mapping=[0, 1])
        ns.sim_run()

        bell_index = swap_program.outcome_as_netsquid_bell_index
        fidelity = ns.qubits.qubitapi.fidelity([qubits_a[1], qubits_b[1]], ns.bell_states[bell_index])

        self.assertAlmostEqual(fidelity, 1 / 2 * (1 + swap_quality), delta=0.01)

    def test_init_gate_duration(self):
        duration = 1000
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=1, T1=0, T2=0,
                                               initialization_duration=duration, )
        node = self.create_node(qdevice_config)

        node.qmemory.execute_instruction(INSTR_INIT, qubit_mapping=[0])

        ns.sim_run()
        self.assertAlmostEqual(ns.sim_time(), duration, delta=0.1)

    def test_single_qubit_gate_duration(self):
        duration = 453
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=1, T1=0, T2=0,
                                               initialization_duration=0,
                                               single_qubit_gate_duration=duration)
        node = self.create_node(qdevice_config)

        node.qmemory.execute_instruction(INSTR_INIT, qubit_mapping=[0])
        ns.sim_run()
        node.qmemory.execute_instruction(INSTR_H, qubit_mapping=[0])
        ns.sim_run()

        self.assertAlmostEqual(ns.sim_time(), duration, delta=0.1)

    def test_two_qubit_gate_duration(self):
        duration = 2
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=1, T1=0, T2=0,
                                               initialization_duration=0,
                                               two_qubit_gate_duration=duration)
        node = self.create_node(qdevice_config)

        node.qmemory.execute_instruction(INSTR_INIT, qubit_mapping=[0])
        ns.sim_run()
        node.qmemory.execute_instruction(INSTR_INIT, qubit_mapping=[1])
        ns.sim_run()
        node.qmemory.execute_instruction(INSTR_CNOT, qubit_mapping=[0, 1])
        ns.sim_run()

        self.assertAlmostEqual(ns.sim_time(), duration, delta=0.1)

    def test_measurement_duration(self):
        duration = 56
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=1, T1=0, T2=0,
                                               initialization_duration=0,
                                               measurement_duration=duration)
        node = self.create_node(qdevice_config)

        node.qmemory.execute_instruction(INSTR_INIT, qubit_mapping=[0])
        ns.sim_run()
        node.qmemory.execute_instruction(INSTR_MEASURE, qubit_mapping=[0])
        ns.sim_run()

        self.assertAlmostEqual(ns.sim_time(), duration, delta=0.1)

    def test_emission_duration(self):
        duration = 66
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=1, T1=0, T2=0,
                                               initialization_duration=0,
                                               emission_duration=duration)
        node = self.create_node(qdevice_config)

        node.qmemory.execute_instruction(INSTR_INIT, qubit_mapping=[0])
        ns.sim_run()
        node.qmemory.execute_instruction(INSTR_EMIT, qubit_mapping=[0, 1])
        ns.sim_run()

        self.assertAlmostEqual(ns.sim_time(), duration, delta=0.1)

    def test_decoherence_trivial_time(self):
        decoherence_time = 50
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=1,
                                               T1=decoherence_time, T2=decoherence_time)
        node = self.create_node(qdevice_config)

        qubits = ns.qubits.create_qubits(2)
        ns.qubits.qubitapi.assign_qstate(qubits, ns.b00)

        node.qmemory.put(qubits[0], positions=[0])

        ns.sim_run(end_time=0.01 * decoherence_time)
        node.qmemory.peek(0)
        fidelity = ns.qubits.qubitapi.fidelity(qubits, ns.b00)

        self.assertAlmostEqual(fidelity, 1, delta=0.01)
        self.assertNotAlmostEqual(fidelity, 1)

    def test_decoherence_t1_large_time(self):
        t1_time = 50
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=1, T1=t1_time, T2=0)
        node = self.create_node(qdevice_config)

        qubits = ns.qubits.create_qubits(2)
        ns.qubits.qubitapi.assign_qstate(qubits, ns.b00)

        node.qmemory.put(qubits[0], positions=[0])

        ns.sim_run(end_time=10 * t1_time)
        node.qmemory.peek(0)
        fidelity = ns.qubits.qubitapi.fidelity(qubits, ns.b00)

        self.assertAlmostEqual(fidelity, 0.5, delta=0.01)

    def test_decoherence_t1_and_t2_large_time(self):
        decoherence_time = 50
        qdevice_config = AbstractQDeviceConfig(num_positions=2, swap_quality=1,
                                               T1=decoherence_time, T2=decoherence_time)
        node = self.create_node(qdevice_config)

        qubits = ns.qubits.create_qubits(2)
        ns.qubits.qubitapi.assign_qstate(qubits, ns.b00)

        node.qmemory.put(qubits[0], positions=[0])

        ns.sim_run(end_time=10 * decoherence_time)
        node.qmemory.peek(0)
        fidelity = ns.qubits.qubitapi.fidelity(qubits, ns.b00)

        self.assertAlmostEqual(fidelity, 0.5, delta=0.01)
