NetSquid-AbstractModel (2.0.0)
================================

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org).

Snippet for various abstract components used in a quantum network

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Documentation
-------------

So build and see the docs see the [docs README](docs/README.md).

Usage
-----

Direct usage
+++++++++++++

This snippet can be used to directly create an abstract quantum processor.

To create an abstract quantum processor, you must use the `AbstractQDeviceConfig` configuration object.


```python
config = AbstractQDeviceConfig(num_positions=2)
abstract_qdevice = AbstractProcessor(config)
```


The ion trap can then be used like any other object of type `netsquid.components.qprocessor.QuantumProcessor`,

Netsquid-netbuilder
++++++++++++++++++++

To integrate the abstract qdevice into netsquid-netbuilder, register the qdevice model as follows:

```python
builder.register("abstract", AbstractQdeviceBuilder, AbstractQDeviceConfig)
```

Once registered, the model can be referenced in the network configuration YAML file by setting `qdevice_typ` to `abstract`.

Contributors
------------

In alphabetical order:

- Adria Labay Mora
- Francisco Ferreira da Silva (f.hortaferreiradasilva[at]tudelft.nl)
- Michał van Hooft (M.K.vanHooft[at]tudelft.nl)

License
-------

**TEMPLATE**: specify the license applicable to your package.

The NetSquid-SnippetTemplate has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
