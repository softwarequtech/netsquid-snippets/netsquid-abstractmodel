PYTHON3      = python3
SOURCEDIR    = netsquid_abstractmodel
TESTDIR      = tests
EXAMPLES     = examples
RUNEXAMPLES  = ${EXAMPLES}/run_examples.py
PIP_FLAGS    = --extra-index-url=https://${NETSQUIDPYPI_USER}:${NETSQUIDPYPI_PWD}@pypi.netsquid.org
MINCOV       = 0
DOCS_DIR     = docs
BUILDDIR     = docs/build

help:
	@echo "install           Installs the package (editable)."
	@echo "verify            Verifies the installation, runs the linter and tests."
	@echo "tests             Runs the tests."
	@echo "examples          Runs the examples and makes sure they work."
	@echo "open-cov-report   Creates and opens the coverage report."
	@echo "lint              Runs the linter."
	@echo "deploy-bdist      Builds and uploads the package to the netsquid pypi server."
	@echo "bdist             Builds the package."
	@echo "deploy-docs       Builds and uploads the documentation to the documentation server."
	@echo "test-deps         Installs the requirements needed for running tests and linter."
	@echo "python-deps       Installs the requirements needed for using the package."
	@echo "docs-deps         Installs the requirements needed for building the documentation."
	@echo "docs              Creates the html documentation"
	@echo "clean             Removes all .pyc files."
	

test-deps:
	@$(PYTHON3) -m pip install -r test_requirements.txt

docs-deps: _check_variables
	@echo -e "\n*** Installing docs requirements for Snippet $(SOURCEDIR)"
	@$(PYTHON3) -m pip install -r $(DOCS_DIR)/requirements.txt ${PIP_FLAGS}

requirements python-deps: _check_variables
	@$(PYTHON3) -m pip install -r requirements.txt ${PIP_FLAGS}

clean:
	@/usr/bin/find . -name '*.pyc' -delete

lint:
	@$(PYTHON3) -m flake8 ${SOURCEDIR} ${TESTDIR} ${EXAMPLEDIR}

tests:
	@$(PYTHON3) -m pytest --cov=${SOURCEDIR} --cov-fail-under=${MINCOV} tests

open-cov-report:
	@$(PYTHON3) -m pytest --cov=${SOURCEDIR} --cov-report html tests && open htmlcov/index.html

examples:
	@${PYTHON3} ${RUNEXAMPLES} > /dev/null && echo "Examples OK!" || echo "Examples failed!"

docs: docs-deps
	@echo -e "\n*** Making html documentation for nippet $(SOURCEDIR)"
	$(PYTHON3) -msphinx -M html $(DOCS_DIR)/ $(BUILDDIR)/

bdist:
	@$(PYTHON3) setup.py bdist_wheel

install: _check_variables test-deps
	@$(PYTHON3) -m pip install -e . ${PIP_FLAGS}

_check_variables:
ifndef NETSQUIDPYPI_USER
	$(error Set the environment variable NETSQUIDPYPI_USER before uploading)
endif
ifndef NETSQUIDPYPI_PWD
	$(error Set the environment variable NETSQUIDPYPI_PWD before uploading)
endif

_clean_dist:
	@/bin/rm -rf dist

deploy-bdist: _clean_dist bdist
	@$(PYTHON3) setup.py deploy

deploy-docs: python-deps docs
	@echo -e "\n*** Uploading docs to documentation server (requires authentication)."
	@$(PYTHON3) setup.py deploy_docs

verify: clean test-deps python-deps lint tests examples _verified

_verified:
	@echo "The snippet is verified :)"

.PHONY: clean lint test-deps python-deps docs-deps tests verify bdist deploy-bdist deploy-docs _clean_dist install open-cov-report examples _check_variables docs
